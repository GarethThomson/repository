# Challenge 5 (bonus)
# Draw a rainbow spiral using Turtle Graphics

import turtle

colors = ['red', 'purple', 'blue', 'green', 'yellow', 'orange']

t = turtle.Turtle()
turtle.bgcolor('black')
for i in range(360):
    t.speed(i)
    t.pencolor(colors[i%6])
    t.forward(i)
    t.left(59)

turtle.done()
