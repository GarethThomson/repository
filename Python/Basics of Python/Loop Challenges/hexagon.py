# Challenge 2
# Use a Loop (it can be a For Loop or a While Loop) to draw a coloured-in hexagon

import turtle

t = turtle.Turtle()
t.color('red', 'blue')
t.width(3)
t.begin_fill()

for i in range(6):
    t.forward(100)
    t.left(60)

t.end_fill()

input()
