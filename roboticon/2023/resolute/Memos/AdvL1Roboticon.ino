int pumpDigitalPin = 3;

int dispenceLevelOneButton = 4;
int dispenceLevelTwoButton = 5;
int dispenceLevelThreeButton = 6;

int BASE_BAUD_RATE = 9600;

int dispensingTime = 0;

void setup() 
{
  Serial.begin(BASE_BAUD_RATE);
  pinMode(pumpDigitalPin, OUTPUT);
  digitalWrite(pumpDigitalPin, LOW);
}


void loop() 
{
  if (digitalRead(2) == LOW)
  {
    sanitize();
  }
  if (digitalRead(dispenceLevelOneButton) == LOW)
  {
    dispensingTime = 1000;
  }
  if (digitalRead(dispenceLevelTwoButton) == LOW)
  {
    dispensingTime = 2000;
  }
  if (digitalRead(dispenceLevelThreeButton) == LOW)
  {
    dispensingTime = 3000;
  }
  delay(80);
}


void sanitize()
{
  Serial.println("Currently Pumping!");

  Serial.println(DespenceTime);

  digitalWrite(pumpDigitalPin, HIGH);

  delay(DespenceTime);

  digitalWrite(pumpDigitalPin, LOW);

  Serial.println("Please make sure your hand is clear from the spout!");

  wait();
}

void wait()
{
  while (digitalRead(2) == LOW)
  { 
    delay(1);
  }
}
