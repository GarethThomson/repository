int sensorAnaloguePin = A0;
int pumpDigitalPin = 5;
int buttonDigitalPin = 6;
int startButtonDigitalPin = 7;
int readInSetPointValue = 0;

int BASE_BAUD_RATE = 9600

void setup()
{
  Serial.begin(BASE_BAUD_RATE);

  pinMode(pumpDigitalPin, OUTPUT);
  pinMode(buttonDigitalPin, INPUT);

  pinMode(startButtonDigitalPin, INPUT);

  Serial.println("Push The Blue Button!");

  int isButtonPressed = digitalRead(buttonDigitalPin);

  while(isButtonPressed == LOW){
    isButtonPressed = digitalRead(buttonDigitalPin);
    Serial.println(analogRead(sensorAnaloguePin));
    delay(100);
  }

  readInSetPointValue = analogRead(sensorAnaloguePin);

  Serial.print("You're set in value is");
  Serial.println(readInSetPointValue);

  Serial.println("Loading... Push Green Button Now To Start");
}

void loop()
{
  
  int readInMoistureLevel = analogRead(sensorAnaloguePin);
  int isStartButtonPressed = digitalRead(startButtonDigitalPin);

  delay(100);
  
  if (isStartButtonPressed == HIGH){
    pourWater();
    Serial.println("DONE!");
    while(true) {};
  }
}

void pourWater() {
  int readInMoistureLevel = analogRead(sensorAnaloguePin);

  while(readInMoistureLevel > (readInSetPointValue-20))
  {
      readInMoistureLevel = analogRead(sensorAnaloguePin);

      Serial.print("Current Read Moisture Level: ");
      Serial.print(readInMoistureLevel);

      Serial.print("\t\t\t");

      Serial.print("Current Set High Point: ");
      Serial.println(readInSetPointValue);
      
      digitalWrite(pumpDigitalPin, HIGH);
      delay(300);
  }
  digitalWrite(pumpDigitalPin, LOW);
}
