int Right_Motor = 2;
int Left_Motor = 3;

int Left_Pin = 4;
int Middle_Pin = 5;
int Right_Pin = 6;

int BASE_BAUD_RATE = 9600


void setup() {
  Serial.begin(BASE_BAUD_RATE);

  pinMode(Right_Motor, OUTPUT);
  pinMode(Left_Motor, OUTPUT);
  
}

void loop() 
{

  int Left_IR = digitalRead(Left_Pin);
  int Mid_IR = digitalRead(Middle_Pin);
  int Right_IR = digitalRead(Right_Pin);

  if (Left_IR == 0 and Mid_IR == 1 and Right_IR == 0) //White:Black:White
  {
    digitalWrite(2, HIGH);
    digitalWrite(3, HIGH);
  }
  else if (Left_IR == 1 and Mid_IR == 0 and Right_IR == 0)//Black:White:White
  {
    digitalWrite(2, HIGH);
    digitalWrite(3, LOW);
  }
  else if (Left_IR == 0 and Mid_IR == 0 and Right_IR == 1)//White:White:Black
  {
    digitalWrite(2, LOW);
    digitalWrite(3, HIGH);
  }
  else if (Left_IR == 1 and Mid_IR == 1 and Right_IR == 0)//Black:Black:White
  {
    digitalWrite(2, HIGH);
    digitalWrite(3, LOW);
  }
  else if (Left_IR == 0 and Mid_IR == 1 and Right_IR == 1)//White:Black:Black
  {
    digitalWrite(2, LOW);
    digitalWrite(3, HIGH);
  }
  else
  {
    //digitalWrite(2, LOW);
    //digitalWrite(3, LOW);
  }
}
