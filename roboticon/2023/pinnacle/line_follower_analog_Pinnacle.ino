// Sensor Variables
int Left_IR = 0
int Mid_IR = 0
int Right_IR = 0

// Threshold values (need to be calculated)
int Left_T = 0;
int Mid_T = 0;
int Right_T = 0;

void setup() {
  Serial.begin(9600);

  pinMode(2, OUTPUT); //Right motor
  pinmode(3, output); //left motor

}

void loop() 
{

  int Left_IR = analogRead(A0);
  int Mid_IR = analogRead(A0);
  int Right_IR = analogRead(A0);

  if (Left_IR < Left_T and Mid_IR > Mid_T and Right_IR < Right_T) //White:Black:White
  {
    digitalWrite(2, HIGH);
    digitalWrite(3, HIGH);
  }
  else if (Left_IR > Left_T and Mid_IR < Mid_T and Right_IR < Right_T)//Black:White:White
  {
    digitalWrite(2, HIGH);
    digitalWrite(3, LOW);
  }
  else if (Left_IR > Left_T and Mid_IR > Mid_T and Right_IR < Right_T)//White:White:Black
  {
    digitalWrite(2, LOW);
    digitalWrite(3, HIGH);
  }
  else if (Left_IR < Left_T and Mid_IR < Mid_T and Right_IR > Right_T)//Black:Black:White
  {
    digitalWrite(2, HIGH);
    digitalWrite(3, LOW);
  }
  else if (Left_IR < Left_T and Mid_IR > Mid_T and Right_IR > Right_T)//White:Black:Black
  {
    digitalWrite(2, LOW);
    digitalWrite(3, HIGH);
  }
  else
  {
    digitalWrite(2, LOW);
    digitalWrite(3, LOW);
  }
}
